using UnityEngine;

namespace AudioCollection
{
    public class PlayAudioCollection : MonoBehaviour
    {
        private AudioSource _audioSource;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }
        
        public void PlayRandomOneShot(AudioClipCollection collection)
        {
            if (collection.collection.Count <= 0)
                return;
            
            int i = Random.Range(0, collection.collection.Count);
            
            _audioSource.PlayOneShot(collection.collection[i]);
        }
    }
}