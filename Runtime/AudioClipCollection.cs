using System.Collections.Generic;
using UnityEngine;

namespace AudioCollection
{
    [CreateAssetMenu(fileName = "AudioClipCollection", menuName = "AudioCollection/ClipCollection", order = 0)]
    public class AudioClipCollection : ScriptableObject
    {
        public List<AudioClip> collection;
    }
}